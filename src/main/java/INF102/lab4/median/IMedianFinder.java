package INF102.lab4.median;

public interface IMedianFinder <T extends Comparable<? super T>> {

    public T getMedian();

    public void add(T element);



}
