package INF102.lab4.median;

import java.util.*;


public class QuickMedian implements IMedian {

    //reference https://stackoverflow.com/questions/1790360/median-of-medians-in-java
    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int listSize = listCopy.size();

        getMedian(listCopy);
        T median = listCopy.get(listSize/2);

        System.out.println(median);
        return median;

    }

    private <T extends Comparable<T>> List<T> getMedian(List<T> list) {
        Random random = new Random();
        int randomIndex = random.nextInt(list.size());
        T foundMedian = null;
        T pivotPoint = list.get(randomIndex);
        List<T> larger = new ArrayList<>();
        List<T> smaller = new ArrayList<>();
        List<T> pivot = new ArrayList<>();

        if (list.size() <= 1) {
            return list;
        } else {

            for (T element : list) {
                int compare = element.compareTo(pivotPoint);
                if (compare < 0) {
                    smaller.add(element);
                } else {
                    if (compare > 0) {
                        larger.add(element);
                    } else {
                        pivot.add(element);
                    }


                    if (smaller.size() > larger.size()) {
                        getMedian(smaller);
                    } else {
                        getMedian(larger);
                    }
                }
            }
        }
        return larger;
    }}






